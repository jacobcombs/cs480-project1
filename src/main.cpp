#include <algorithm>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

std::vector<uint8_t> tokenize(const std::string& S) {
    std::vector<uint8_t> results;
    std::stringstream input(S);
    int temp;
    while (input >> temp)
        results.push_back(temp);
    return results;
}

std::string stringify(const std::vector<uint8_t>& V) {
    std::string result;
    std::stringstream output;
    for (std::vector<uint8_t>::const_iterator ii = V.cbegin();
         ii != V.cend(); ii++)
        output << std::setw(4) << static_cast<int>(*ii) << " ";
    result = output.str();
    return result;
}

std::vector<std::vector<uint8_t> > get_children(std::vector<uint8_t> V) {
    std::vector<std::vector<uint8_t> > results;
    for (size_t i = 2; i <= V.size(); i++)
        for (size_t j = 0; j < V.size() - i + 1; j++) {
            results.push_back(V);
            std::reverse(results.back().begin() + j,
                         results.back().begin() + j + i);
        }
    return results;
}

uint8_t get_number_break_pts(std::vector<uint8_t> V) {
    int ct = 0;
    for (std::vector<uint8_t>::const_iterator ii = V.cbegin();
         ii + 1 != V.cend(); ii++)
        if (std::abs(*ii - *(ii + 1)) > 1)
            ct++;
    return ct;
}

struct Node {
    std::vector<uint8_t> permutation;
    std::string str;
    uint8_t break_pts;
    int depth;
    int f_value;
    Node* parent;

    void set_parent(Node* P) {
        parent = P;
        depth = parent == nullptr ? 0 : parent->depth + 1;
        f_value = 2 * depth + break_pts;
    }

    Node(std::vector<uint8_t> V, std::string S, Node* P) {
        permutation = V;
        str = S;
        break_pts = get_number_break_pts(permutation);
        set_parent(P);
    }
};

bool greater_node(Node* lhs, Node* rhs) {
    if (lhs->f_value > rhs->f_value) return true;
    if (lhs->f_value < rhs->f_value) return false;
    if (lhs->str > rhs->str) return true;
    if (lhs->str < rhs->str) return false;
    return false;
}

class hash_heap {
    std::vector<Node*> heap_vector;
    std::unordered_map<std::string, int> hash_table;

 public:
    bool empty() {
        return heap_vector.empty();
    }

    int get_depth(std::string S) {
        std::unordered_map<std::string, int>::iterator ii = hash_table.find(S);
        if (ii == hash_table.end())
            return -1;
        return ii->second;
    }

    Node* delete_min() {
        Node* min;
        if (heap_vector.empty())
            return nullptr;
        std::pop_heap(heap_vector.begin(), heap_vector.end(), greater_node);
        min = heap_vector.back();
        heap_vector.pop_back();
        hash_table.erase(min->str);
        return min;
    }

    void insert(Node* to_insert) {
        heap_vector.push_back(to_insert);
        std::push_heap(heap_vector.begin(), heap_vector.end(), greater_node);
        hash_table[to_insert->str] = to_insert->depth;
    }

    void find_and_update(std::string S, Node* P) {
        std::vector<Node*>::iterator ii = heap_vector.begin();
        while (ii != heap_vector.end() && (*ii)->str != S) ii++;
        (*ii)->set_parent(P);
        hash_table[S] = (*ii)->depth;
        std::make_heap(heap_vector.begin(), heap_vector.end(), greater_node);
    }
};

int flip_sort(Node* start, std::vector<std::string>* path) {
    int nodes_expanded = 0;

    hash_heap open_set;
    open_set.insert(start);

    while (!open_set.empty()) {
        Node* X = open_set.delete_min();
        nodes_expanded++;

        if (std::is_sorted(X->permutation.begin(), X->permutation.end())) {
            path->resize(X->depth + 1);
            std::vector<std::string>::reverse_iterator ii = path->rbegin();
            while (ii != path->rend()) {
                *ii++ = X->str;
                X = X->parent;
            }
            break;
        }

        std::vector<std::vector<uint8_t> > chi = get_children(X->permutation);
        for (std::vector<std::vector<uint8_t> >::iterator ii = chi.begin();
             ii != chi.end(); ii++) {
            std::string child_str = stringify(*ii);
            int depth = open_set.get_depth(child_str);
            if (depth == -1)
                open_set.insert(new Node(*ii, child_str, X));
            else if (depth < X->depth + 1)
                open_set.find_and_update(child_str, X);
        }
    }
    return nodes_expanded;
}

void print_solution(std::vector<std::string> path) {
    std::vector<uint8_t> current, previous;
    std::vector<std::string>::iterator ii = path.begin();
    previous = tokenize(*ii++);
    while (ii != path.end()) {
        current = tokenize(*ii);
        int left_diff = previous.size();
        int right_diff = 0;
        for (int index = 0; index < previous.size(); index++)
            if (previous[index] != current[index]) {
                if (index < left_diff)
                    left_diff = index;
                if (index > right_diff)
                    right_diff = index;
            }
        (*ii)[1 + 5 * left_diff] = '[';
        (*ii)[4 + 5 * right_diff] = ']';
        previous = current;
        ii++;
    }

    ii = path.begin();
    std::cout << "Input ...." << *ii++ << std::endl;
    int move = 1;
    while (ii != path.end())
        std::cout << "Move " << move++ << " ..." << *ii++ << std::endl;
}

int main() {
    std::string start_str;
    std::cout << "Enter a (space-delimited) permutation:" << std::endl;
    std::getline(std::cin, start_str);

    std::vector<uint8_t> start_vec = tokenize(start_str);
    start_str = stringify(start_vec);
    Node* start = new Node(start_vec, start_str, nullptr);

    std::vector<std::string> path;
    clock_t timer;
    std::cout << "Conducting A-star search... " << std::flush;
    timer = clock();

    int node_ctr = flip_sort(start, &path);

    timer = clock() - timer;
    std::cout << "Done." << std::endl;
    std::cout << "Expanded " << node_ctr << " nodes in about "
              << timer / CLOCKS_PER_SEC << " seconds." << std::endl;
    std::cout << "A shortest move sequence (" << path.size() - 1
              << " flips) is:" << std::endl;
    print_solution(path);

    return 0;
}
